<?php
session_start();
include 'connection.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Content</title>
</head>
<body>
<div id="main">
<div id="header" align="center">
    <table>
        <tr>
        	<td><b>Welcome, <?php echo $_SESSION["username"]?>!</b></td>
        	<td>
        	<?php
			if ($_SESSION['isAdmin']){
			echo "<form action='control.php'><input type='submit' name='act' size='15' value='create'></form>";
			} 
			?></td>
            <td><form method="post"><input type='submit' name='logout' size="15" value="logout"></form></td>
        </tr>
    </table>
</div><!-- #header -->
<?php
if (isset($_POST['logout'])){
    session_unset('isAdmin');
    session_unset('username');
    session_unset('message');
    session_destroy();
    header("Location: index.php"); exit();
} 

if ($_SESSION['isAdmin']){
	include 'table.php';
	} 
?>
<div id="content" align="center">
	<h1>Cut me!</h1>
	<form method='post'>
		<input type='text' name='fullname' size='40'>
		<input type='submit' name='submit_cut' value='Cut me!'>
	</form>
<?php
if (isset($_POST['submit_cut'])) {
	$statement = $connection->query('SELECT * FROM links WHERE fullname="' . $_POST['fullname'] . '"');
	$row = $statement->fetch(PDO::FETCH_ASSOC);
    if (empty($row)){
		$hash = '';
		while (!$hash) {
			$hash = mb_strimwidth(md5($_POST['fullname']), rand(0,24), 8);				
			foreach ($connection->query('SELECT * FROM links') as $row) {
				if ($row['hash'] == $hash) {
					$hash = '';
				}
			}
		}
		$query= 'INSERT INTO links (fullname, shortname, hash) 
		  VALUES ("' . strip_tags($_POST['fullname']) . '", "cut.me/' . $hash . '", "' . $hash . '")';
		$connection->exec($query);
		echo "Link added! --> <a href='cut.me/" . $hash . "'>cut.me/" . $hash . "</a> <---";
	} else {
		echo "Already cut! --> <a href='" . $row['shortname'] . "''>" . $row['shortname'] . "</a> <---";
	}
}

$statement = $connection->query('SELECT * FROM links');
echo "<table border=\"1\">\n";
while($row = $statement->fetch(PDO::FETCH_ASSOC)) {
    echo "<tr>";
    echo "<td>" . $row['id'] . "</td>";
    echo "<td>" . $row['fullname'] . "</td>";
    echo "<td>" . $row['shortname'] . "</a></td>";
    echo "</tr>";
}
echo "</table>";



$connection = null;
?>
</div><!-- #content -->
</div><!-- #main -->
</body> 	
</html>
