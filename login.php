<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Users control</title>
</head>
<body>
<div id='message' align='center'>
<?php
include 'user.classes';
include 'connection.php';

if (!empty($_POST['name'])){
    $statement = $connection->query('SELECT * FROM users WHERE name="'.$_POST['name'].'"');
	$statement->setFetchMode(PDO::FETCH_CLASS, 'User');
	$row = $statement->fetch(PDO::FETCH_CLASS);
    if (empty($row)){
        echo "Unknown user";
    } elseif ($_POST['passwd'] != $row->getPasswd()){
        echo "Wrong password";
    } else {
        $_SESSION["isAdmin"] = $row->isAdmin();
        $_SESSION["username"] = $row->getName();
        header("Location: content.php"); exit();
    }
}
else {
    echo "Empty fields";
}
echo "<br><a href='/index.php'>На главную</a>";

?>
</div><!-- #message -->
</body>
</html>

