<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Users control</title>
</head>
<body>
<div id='control' align='center'>
<?php
include 'connection.php';

if (isset($_GET['id'])) {
	$id = $_GET['id'];
}
if (isset($_GET['act'])) {
	$act = $_GET['act'];
}

switch ($act) {

	case 'chname':
		echo "<form method='post'><input type='text' name='chname' size='10'>
		  <input type='submit' name='submit' value='Сменить имя'></form>
		  <br><a href='/content.php'>На главную</a>";
		break;
	
	case 'chpasswd':
		echo "<form method='post'><input type='text' name='chpasswd' size='10'>
		  <input type='submit' name='submit' value='Сменить пароль'></form>
		  <br><a href='/content.php'>На главную</a>";
		break;	
	
	case 'delete':
		$query = "DELETE FROM users WHERE id=".$id." AND isadmin <> 1";
		$connection -> exec($query);
		header("Location: content.php"); exit();
		break;	
	
	case 'create':
		echo "<form method='post'><input type='text' name='newname' size='15'>Имя<br>
		  <input type='password' name='newpasswd' size='15'>Пароль<br>
		  <input type='submit' name='newcreate' size='15' value='Создать'></form><br>";
		if (isset($_SESSION['message'])) {
			echo $_SESSION['message'];
			unset($_SESSION['message']);
		}
		break;
	
	default:
		$_SESSION['message'] = 'default';
		echo $_SESSION['message'];
		unset($_SESSION['message']);
		break;
}


if (isset($_POST['chname'])){
    $connection->exec('UPDATE users SET name="' . strip_tags($_POST["chname"]) . '" WHERE id='.$id);
	header("Location: content.php"); exit();
} elseif (isset($_POST['chpasswd'])) {
    $connection->exec('UPDATE users SET passwd="' . strip_tags($_POST["chpasswd"]) . '" WHERE id='.$id);
	header("Location: content.php"); exit();
} elseif (isset($_POST['newcreate'])){
    if (!empty($_POST['newname']) and !empty($_POST['newpasswd'])){
		$st = $connection->query('SELECT * FROM users WHERE name="' . $_POST['newname'] . '"');
		if ($st->fetch(PDO::FETCH_ASSOC)){
		  	$_SESSION['message'] = "Пользователь уже существует.";
		  	header('location:'.$_SERVER['HTTP_REFERER']);
	    }
	    else {	   
		   	$st = $connection->query('SELECT * FROM users WHERE isadmin = 1');
			if ($st->fetch(PDO::FETCH_ASSOC)) {
			$query = 'INSERT INTO users (name, passwd, isadmin) 
			  VALUES ("' . strip_tags($_POST['newname']) . '","' . strip_tags($_POST['newpasswd']) . '",0)';
			}
		    else {
			$query = 'INSERT INTO users (name, passwd, isadmin) 
			  VALUES ("' . strip_tags($_POST['newname']) . '","' . strip_tags($_POST['newpasswd']) . '",1)';
			}
			$connection -> exec($query);
			if (isset($_GET['reg'])) {
				header("Location: index.php"); 
				$_SESSION['message'] = "Пользователь " . strip_tags($_POST['newname']) . " успешно создан";
			} else {
				header("Location: content.php"); exit();
			}
		}
	} else { 
	  	$_SESSION['message'] = "Попробуйте еще раз. Заполните все поля.";
	  	header('location:'.$_SERVER['HTTP_REFERER']);
	}
}

$connection = null;
?>
</div><!-- #control -->
</body> 	
</html>