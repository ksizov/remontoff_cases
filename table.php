<div id="table" align="center">
    <p><b>Users control</b></p>
<table border="1">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Passwd</th>
        <th>newName</th>
        <th>newPassword</th>
        <th>isAdmin</th>
        <th>KillBill</th>
    </tr>
<?php
include 'user.classes';	

$statement = $connection->query('SELECT * FROM users');
$statement->setFetchMode(PDO::FETCH_CLASS, 'User');

while($row = $statement->fetch(PDO::FETCH_CLASS)) {
    echo "<tr>";
    echo "<td>".$row->getId()."</td>";
    echo "<td>".$row->getName()."</td>";
    echo "<td>".$row->getPasswd()."</td>";
    echo "<td><a href='/control.php?id=".$row->getId()."&act=chname'>Change name</a></td>";
    echo "<td><a href='/control.php?id=".$row->getId()."&act=chpasswd'>Change password</a></td>";
    echo "<td>".$row->isAdmin()."</td>";
    if ($row->isAdmin()) {
        echo "<td></td>";
    } else {
        echo "<td><a href='/control.php?id=".$row->getId()."&act=delete'>Delete</a></td>";
    }
    echo "</tr>";
}
echo "</table>";
?>
</div><!-- #table -->
